//
//  AddRecipeViewController.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import SwiftyXMLParser

protocol AddRecipeVCDelegate: class {
    func newRecipeAdded(recipe: RecipeModel?)
}

class AddRecipeViewController: UIViewController, HeaderViewDelegate {

    var objRecipe: RecipeModel? = nil
    var isEditingEnabled: Bool = false
    weak var delegate : AddRecipeVCDelegate? = nil
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnAddRecipe: UIButton!
    @IBOutlet weak var txtRecipeType: UITextField!
    @IBOutlet weak var imgViewRecipe: UIImageView!
    @IBOutlet weak var btnAddRecipeImage: UIButton!
    @IBOutlet weak var tbleViewAddRecipe: UITableView!
        
    private var arrOfSteps: [String]       = [String]()
    private var pickerView: UIPickerView?  = nil
    private var arrOfRecipeTypes: [String] = [String]()
    private var arrOfIngredients: [String] = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrOfRecipeTypes = Utils.getRecipeTypes()
        self.setupUI()
        self.setupTableView()
        self.createPickerView()
        self.txtRecipeType.inputView = pickerView
        self.txtRecipeType.inputAccessoryView = self.getToolBar()
    }
    
    //MARK:- Helper Methods
    private func setupUI() {
        self.imgViewRecipe.contentMode = .scaleAspectFill
        self.imgViewRecipe.layer.cornerRadius = 50.0
        self.btnAddRecipe.layer.cornerRadius = 10.0
        
        guard let recipe = self.objRecipe else {
            return
        }
        
        self.imgViewRecipe.image = UIImage.init(data: recipe.recipeImageData!)
        self.txtTitle.text = recipe.title
        self.arrOfSteps = recipe.steps ?? [String]()
        self.txtRecipeType.text = recipe.type
        self.arrOfIngredients = recipe.ingredients ?? [String]()
        self.tbleViewAddRecipe.reloadData()
        
        
        self.btnAddRecipe.isHidden      = !self.isEditingEnabled
        self.btnAddRecipeImage.isHidden = !self.isEditingEnabled
        
        self.txtTitle.isUserInteractionEnabled      = self.isEditingEnabled
        self.btnAddRecipe.isUserInteractionEnabled  = self.isEditingEnabled
        self.txtRecipeType.isUserInteractionEnabled = self.isEditingEnabled
    }
    
    private func createPickerView() {
        pickerView = UIPickerView.init()
        pickerView?.dataSource = self
        pickerView?.backgroundColor = UIColor.white
        pickerView?.showsSelectionIndicator = true
    }
    
    private func getToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: TOOL_BAR_BUTTON.Done.rawValue, style: UIBarButtonItem.Style.plain, target: self, action: #selector(doneButtonAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: TOOL_BAR_BUTTON.Cancel.rawValue, style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelButtonAction))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    private func setupTableView() {
        self.tbleViewAddRecipe.tableFooterView = UIView.init(frame: .zero)
        self.tbleViewAddRecipe.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    private func showAddRecipeAlert(_ type: Int) {
        let alertController = UIAlertController.init(title: type == 0 ? "Add Ingredient" : "Add Step", message: nil, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = type == 0 ? "Ingredient" : "Step"
        }
        
        let addAction = UIAlertAction.init(title: "Add", style: .default) { (alert) in
            alertController.dismiss(animated: true) {
                let value = alertController.textFields?[0].text
                
                if value != "" || value != nil {
                    _ = type == 0 ? self.arrOfIngredients.append(value!) : self.arrOfSteps.append(value!)
                }
                
                DispatchQueue.main.async {
                    self.tbleViewAddRecipe.reloadData()
                }
            }
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .default) { (alert) in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func validateRecipe() -> Bool {
        if(self.txtTitle.text == "" || self.txtTitle.text == nil) {
            AlertManager.showAlertWithMessage(message: ERROR_MESSAGES.RecipeTitleNotFound.rawValue)
            return false
        }
        else if(self.txtRecipeType.text == "" || self.txtRecipeType.text == nil) {
            AlertManager.showAlertWithMessage(message: ERROR_MESSAGES.RecipeTypeNotFound.rawValue)
            return false
        }
        else if(self.arrOfIngredients.count == 0) {
            AlertManager.showAlertWithMessage(message: ERROR_MESSAGES.RecipeIngredientsNotFound.rawValue)
            return false
        }
        else if(self.arrOfSteps.count == 0) {
            AlertManager.showAlertWithMessage(message: ERROR_MESSAGES.RecipeStepsNotFound.rawValue)
            return false
        }
        else {
            return true
        }
    }
    
    //MARK:- Button Action Methods
    @IBAction func buttonActionAddRecipeImage(_ sender: Any) {
        ImagePickerManager.shared.showAttachmentActionSheet(vc: self)
        ImagePickerManager.shared.imageUrlPickedBlock = { (imageUrl, image) in

            DispatchQueue.main.async(execute: {
                self.imgViewRecipe.image = image
            })
        }
    }
    
    @IBAction func buttonActionAddRecipe(_ sender: Any) {
        //validation
        if(self.validateRecipe()) {
            var recipe = RecipeModel()
            
            recipe.type = self.txtRecipeType.text
            recipe.steps = self.arrOfSteps
            recipe.title = self.txtTitle.text
            recipe.ingredients = self.arrOfIngredients
            recipe.recipeImageData = self.imgViewRecipe.image?.jpegData(compressionQuality: 1.0)
            
            self.delegate?.newRecipeAdded(recipe: recipe)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
        guard let selectedRow = self.pickerView?.selectedRow(inComponent: 0) else {
            return
        }
        self.txtRecipeType.text = self.arrOfRecipeTypes[selectedRow]
    }

    @objc func cancelButtonAction() {
        self.view.endEditing(true)
    }
    
    //MARK:- Delegate Methods
    func addButtonDidSelected(type: HeaderViewType) {
        switch type {
            case .ingredient:
                self.showAddRecipeAlert(0)
            case .steps:
                self.showAddRecipeAlert(1)
        }
    }
}

extension AddRecipeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
            case 0:
                return self.arrOfIngredients.count
            case 1:
                return self.arrOfSteps.count
            default:
                return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        }
        let value = indexPath.section == 0 ? self.arrOfIngredients[indexPath.row] : self.arrOfSteps[indexPath.row]
        cell?.textLabel?.text = value
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView.loadNibFromXIB()
        headerView?.lblTitle.text = section == 0 ? "Add Ingredient" : "Add Step"
        headerView?.type = section == 0 ? .ingredient : .steps
        headerView?.delegate = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
}

extension AddRecipeViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrOfRecipeTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrOfRecipeTypes[row]
    }
}
