//
//  ViewController.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import SwiftyXMLParser

class RecipeViewController: UIViewController, AddRecipeVCDelegate {

    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblFilterType: UILabel!
    @IBOutlet weak var tbleViewRecipes: UITableView!
    @IBOutlet weak var lblNoRecipesFound: UILabel!
    
    private var arrOfRecipes : [RecipeModel] = [RecipeModel]()
    private var arrOfAllRecipes : [RecipeModel] = [RecipeModel]() //used for filtering (maintain all recipes for filtering)
    
    //MARK:- View LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.btnClear.layer.cornerRadius = 10.0
        self.btnFilter.layer.cornerRadius = 10.0
        self.arrOfAllRecipes = CoreDataManager.sharedInstance().fetchRecipes()
        self.arrOfRecipes = self.arrOfAllRecipes
        self.tbleViewRecipes.reloadData()
    }
    
    //MARK:- Button Action Methods
    @IBAction func addRecipeButtonAction(_ sender: Any) {
        self.navigateToAddRecipe(recipeObject: nil, forEditing: true)
    }
    
    @IBAction func buttonActionClear(_ sender: Any) {
        self.arrOfRecipes = CoreDataManager.sharedInstance().fetchRecipes()
        self.tbleViewRecipes.reloadData()
    }
    
    @IBAction func buttonActionFilter(_ sender: Any) {
        let alertController = UIAlertController.init(title: "Select Recipe Type", message: "", preferredStyle: .actionSheet)
        let arrOfRecipeTypes = Utils.getRecipeTypes()
        if arrOfRecipeTypes.count > 0 {
            
            for recipeType in arrOfRecipeTypes {
                let alertAction = UIAlertAction.init(title: recipeType, style: .default) { (alert) in
                    self.filterRecipesWith(recipeType: alert.title)
                }
                alertController.addAction(alertAction)
            }
            
            let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (alert) in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(cancelAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Delegate Methods
    func newRecipeAdded(recipe: RecipeModel?) {
        guard let objRecipe = recipe else {
            return
        }
        self.arrOfRecipes.append(objRecipe)
        self.tbleViewRecipes.reloadData()
        CoreDataManager.sharedInstance().saveRecipe(recipe: objRecipe)
    }
    
    //MARK: Helper Methods
    private func filterRecipesWith(recipeType: String?) {
        if let type = recipeType {
            DispatchQueue.main.async {
                self.arrOfRecipes = self.arrOfAllRecipes.filter({ $0.type == type })
                self.tbleViewRecipes.reloadData()
            }
        }
        else {
            DispatchQueue.main.async {
                self.arrOfRecipes = self.arrOfAllRecipes
                self.tbleViewRecipes.reloadData()
            }
        }
    }
    
    private func navigateToAddRecipe(recipeObject: RecipeModel?, forEditing: Bool) {
        let addRecipeVC = Utils.getControllerWith(className: AddRecipeViewController.self, storyboardName: STORYBOARDS.main) as? AddRecipeViewController
        addRecipeVC?.delegate = self
        addRecipeVC?.objRecipe = recipeObject
        addRecipeVC?.isEditingEnabled = forEditing
        self.navigationController?.pushViewController(addRecipeVC!, animated: true)
    }
    
    private func setupTableView() {
        self.tbleViewRecipes.register(UINib(nibName: CELLS.RecipeCell.rawValue, bundle: nil), forCellReuseIdentifier: CELLS.RecipeCell.rawValue)
        self.tbleViewRecipes.tableFooterView = UIView.init(frame: .zero)
        self.tbleViewRecipes.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

extension RecipeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tbleViewRecipes.isHidden = self.arrOfRecipes.count == 0 // ? true : false
        self.lblNoRecipesFound.isHidden = self.arrOfRecipes.count != 0 // ? false : true
        return self.arrOfRecipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELLS.RecipeCell.rawValue, for: indexPath) as? RecipeCell else {
            return UITableViewCell()
        }
        let objRecipe = self.arrOfRecipes[indexPath.row]
        cell.setupCellUI(objRecipe)
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction.init(style: .destructive, title: "Delete") { (action, indexPath) in
            CoreDataManager.sharedInstance().deleteRecipe(recipe: self.arrOfRecipes[indexPath.row])
            self.arrOfRecipes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .top)
        }
        
        let editAction = UITableViewRowAction.init(style: .normal, title: "Edit") { (action, indexPath) in
            self.navigateToAddRecipe(recipeObject: self.arrOfRecipes[indexPath.row], forEditing: true)
        }
        return [deleteAction,editAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateToAddRecipe(recipeObject: self.arrOfRecipes[indexPath.row], forEditing: false)
    }
}

