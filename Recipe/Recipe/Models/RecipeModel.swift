//
//  RecipeModel.swift
//  RecipeModel
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import CoreData
import Foundation

struct RecipeModel : Codable {
    
    var type : String? = ""
    var title : String? = ""
    var steps : [String]? = [String]()
    var ingredients : [String]? = [String]()
    var recipeImageData : Data? = nil
    
    init() {
    }
    
    enum CodingKeys: String, CodingKey {
        
        case type = "type"
        case title = "title"
        case steps = "steps"
        case ingredients = "ingredients"
        case recipeImageData = "recipeImageData"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        steps = try values.decodeIfPresent([String].self, forKey: .steps) ?? [String]()
        ingredients = try values.decodeIfPresent([String].self, forKey: .ingredients) ?? [String]()
        recipeImageData = try values.decodeIfPresent(Data.self, forKey: .recipeImageData)
    }
}
