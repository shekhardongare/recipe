//
//  RecipeCell.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewRecipe: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgViewRecipe.layer.cornerRadius = self.imgViewRecipe.frame.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCellUI(_ objRecipe: RecipeModel) {
        self.lblTitle?.text = objRecipe.title ?? ""
        self.imgViewRecipe.image = UIImage.init(data: objRecipe.recipeImageData!)
    }
}
