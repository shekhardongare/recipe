//
//  CoreDataManager.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import CoreData
import Foundation

class CoreDataManager: NSObject {
    
    private static var sharedinstance : CoreDataManager?
    
    //MARK:-  SharedInstance
    class func sharedInstance() -> CoreDataManager {
       if(sharedinstance == nil) {
           sharedinstance = CoreDataManager();
       }
       return sharedinstance!
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Recipe")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK:- Helper Methods
    func saveRecipe(recipe: RecipeModel) {
        let entity = NSEntityDescription.entity(forEntityName: "Recipe", in: persistentContainer.viewContext)!
        let objRecipe = NSManagedObject.init(entity: entity, insertInto: persistentContainer.viewContext)
        
        let stepsStr = recipe.steps?.joined(separator: ",")
        let ingredientStr = recipe.ingredients?.joined(separator: ",")
        
        objRecipe.setValue(recipe.type, forKey: "type")
        objRecipe.setValue(recipe.title, forKey: "title")
        objRecipe.setValue(stepsStr, forKey: "steps")
        objRecipe.setValue(ingredientStr, forKey: "ingredients")
        objRecipe.setValue(recipe.recipeImageData, forKey: "recipeImageData")
        self.saveContext()
    }
    
    func fetchRecipes() -> [RecipeModel] {
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Recipe")
        do {
            let recipes = try persistentContainer.viewContext.fetch(fetchRequest)
            return self.getRecipeArrFrom(recipes)
        } catch _ as NSError {
            return [RecipeModel]()
        }
    }
    
    func deleteRecipe(recipe: RecipeModel) {
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Recipe")
        fetchRequest.predicate = NSPredicate.init(format: "title == %@", recipe.title!)
        do {
            let recipes = try persistentContainer.viewContext.fetch(fetchRequest)
            if recipes.count > 0 {
                persistentContainer.viewContext.delete(recipes.last!)
                self.saveContext()
            }
        } catch let error as NSError {
            debugPrint("Error: ", error)
        }
    }
    
    private func getRecipeArrFrom(_ recipes:[NSManagedObject]) -> [RecipeModel] {
        
        var resultArr = [RecipeModel]()
        for rec in recipes {
            var recipObj = RecipeModel()
            recipObj.type = rec.value(forKey: "type") as? String
            recipObj.title = rec.value(forKey: "title") as? String
            recipObj.recipeImageData = rec.value(forKey: "recipeImageData") as? Data
            
            let stepsStr = rec.value(forKey: "steps") as? String
            let ingredientStr = rec.value(forKey: "ingredients") as? String
            
            let stepsArr = stepsStr?.components(separatedBy: ",")
            let ingredientsArr = ingredientStr?.components(separatedBy: ",")
            
            recipObj.steps = stepsArr
            recipObj.ingredients = ingredientsArr
            resultArr.append(recipObj)
        }
        return resultArr
    }
}
