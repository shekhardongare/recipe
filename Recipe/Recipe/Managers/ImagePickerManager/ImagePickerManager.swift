//
//  ImagePickerManager.swift
//  AirSprint
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ImagePickerManager: NSObject {
    static let shared = ImagePickerManager()
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var imageUrlPickedBlock: ((String, UIImage?) -> Void)?
    
    enum AttachmentType: String {
        case camera, photoLibrary
    }
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Add a File"
        static let actionFileTypeDescription = "Choose a option to add image"
        static let camera = "Camera"
        static let phoneLibrary = "Phone Library"
        
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
        static let IMAGE_COMPRESSION_QUALITY : CGFloat = 0.75
    }
    
    
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    private func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController) {
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
        case .denied:
            debugPrint("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            debugPrint("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    debugPrint("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.photoLibrary()
                    }
                }else{
                    debugPrint("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            debugPrint("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            DispatchQueue.main.async(execute: {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                myPickerController.modalPresentationStyle = .fullScreen
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            })
        }
    }
        
    //MARK: - PHOTO PICKER
    private func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            DispatchQueue.main.async(execute: {

                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                myPickerController.modalPresentationStyle = .fullScreen
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            })
        }
    }
    
    //MARK: - SETTINGS ALERT
    private func addAlertForSettings(_ attachmentTypeEnum: AttachmentType) {
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController.modalPresentationStyle = .fullScreen
        DispatchQueue.main.async(execute: {
            self.currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
        })
        
    }
}

//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsibel for canceling the picker
extension ImagePickerManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
            if(picker.sourceType == .camera) {
                let fileManager = FileManager.default
                let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
                let imagePath = documentsPath?.appendingPathComponent("image.jpg")
                let imageData = image.jpegData(compressionQuality: Constants.IMAGE_COMPRESSION_QUALITY)
                try! imageData?.write(to: imagePath!)
                self.imageUrlPickedBlock?(imagePath!.absoluteString, UIImage.init(data: imageData!))
                currentVC?.dismiss(animated: true, completion: nil)
            }
        }
        else {
            debugPrint("Something went wrong in  image")
        }
        
        if #available(iOS 11.0, *) {
            if let imageUrl = info[UIImagePickerController.InfoKey.imageURL.rawValue] as? URL {
                let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
                let data = image?.jpegData(compressionQuality: Constants.IMAGE_COMPRESSION_QUALITY)
                self.imageUrlPickedBlock?(imageUrl.absoluteString, UIImage.init(data: data!))
                currentVC?.dismiss(animated: true, completion: nil)
            }
        }
        else {
            if let imageUrl = info[UIImagePickerController.InfoKey.referenceURL.rawValue] as? URL {
                let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
                let data = image?.jpegData(compressionQuality: Constants.IMAGE_COMPRESSION_QUALITY)
                self.imageUrlPickedBlock?(imageUrl.absoluteString, UIImage.init(data: data!))
                currentVC?.dismiss(animated: true, completion: nil)
            }
        }
    }
}
