//
//  AlertManager.swift
//  AirSprint
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import Foundation

private let defaultButtonTitle = "OK"

class AlertManager : NSObject {
    
    class func showAlertWithMessage(message : String, buttons : [String]?,completionBlock:((_ buttonIndex : Int, _ alert: UIAlertController) -> Void)?) {
        AlertManager.showAlert(title: nil, message: message, buttons: buttons, completionBlock: completionBlock)
    }
    
    class func showAlertWithTitle(title : String,message : String) {
        AlertManager.showAlert(title: title, message: message,buttons: nil, completionBlock:nil)
    }
    
    class func showAlertWithMessage(message : String) {
        AlertManager.showAlert(title: nil, message: message,buttons: nil, completionBlock:nil)
    }
    
    private class func showAlert( title : String?,message : String, buttons : [String]?,completionBlock:((_ buttonIndex : Int, _ alert: UIAlertController) -> Void)?) {
        let vc =  UIApplication.shared.keyWindow?.rootViewController
        self.showAlert(title: title, message: message, buttons: buttons, viewController: vc, completionBlock: completionBlock)
    }
    
    private class func showAlert( title : String?,message : String, buttons : [String]?, viewController: UIViewController?, completionBlock:((_ buttonIndex : Int, _ alert: UIAlertController) -> Void)?) {
        
        var alertTitle = title
        if(title == nil) {
            alertTitle = "Recipe"
        }
        
        let alert=UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
        if(buttons == nil) {
            alert.addAction(UIAlertAction(title: defaultButtonTitle, style: UIAlertAction.Style.cancel, handler: {(action:UIAlertAction) in
                completionBlock?(0,alert)
                
            }))
        }
        else {
            var titleCount = 0
            for buttonTitle in buttons!{
                if(titleCount == 0){
                    titleCount += 1
                    alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.cancel, handler: {(action:UIAlertAction) in
                        completionBlock?((buttons?.firstIndex(of: buttonTitle))!,alert)
                        
                    }))
                }
                else {
                    alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
                        completionBlock?(titleCount,alert)
                    }))
                }
            }
        }
        viewController?.present(alert, animated: true, completion: nil)
    }
}
