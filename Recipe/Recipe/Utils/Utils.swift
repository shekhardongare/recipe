//
//  StoryboardUtils.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import UIKit
import Foundation
import SwiftyXMLParser

class Utils: NSObject {
    
    static func getControllerWith(className: AnyObject, storyboardName: STORYBOARDS) -> UIViewController {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: NSStringFromClass(className as! AnyClass).components(separatedBy: ".").last!)
        return viewController
    }
    
    static func getRecipeTypes() -> [String] {
        var arrOfRecipeTypes = [String]()
        let path = Bundle.main.url(forResource: "recipeTypes", withExtension: "xml")
        do {
            let data = try Data.init(contentsOf: path!)
            
            let xml = XML.parse(data)
            for type in xml["RecipeTypes", "Type"] {
                guard let type = type.element?.text else {
                    return arrOfRecipeTypes
                }
                arrOfRecipeTypes.append(type)
            }
            return arrOfRecipeTypes
        }
        catch {
            debugPrint("Error:")
            return arrOfRecipeTypes
        }
    }
}
