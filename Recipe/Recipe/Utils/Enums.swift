//
//  Enums.swift
//  Recipe
//
//  Created by Shekhar on 15/03/20.
//  Copyright © 2020 Shekhar. All rights reserved.
//

import Foundation

enum STORYBOARDS : String {
    case main = "Main"
}

enum XIBS: String {
    case HeaderView = "HeaderView"
}

enum HeaderViewType {
    case steps
    case ingredient
}

enum TOOL_BAR_BUTTON: String {
    case Done   = "Done"
    case Cancel = "Cancel"
}

enum ERROR_MESSAGES: String {
    case RecipeTypeNotFound         = "Please select Recipe Type"
    case RecipeTitleNotFound        = "Please add Recipe Title"
    case RecipeStepsNotFound        = "Please add Recipe Steps"
    case RecipeIngredientsNotFound  = "Please add Recipe Ingredients"
}

enum CELLS: String {
    case RecipeCell = "RecipeCell"
}

